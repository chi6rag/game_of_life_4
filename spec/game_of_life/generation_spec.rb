module GameOfLife
  RSpec.describe Generation do

    describe 'evolution' do
      it "with 1 live cell and 0 live neighbours kills the cell" do
        generation = Generation.new([Cell.new(0, 0, true), Cell.new(0, 1, false),
                                     Cell.new(1, 0, false), Cell.new(1, 1, false)])
        next_generation = generation.evolve
        expect(next_generation.cells).to eq([Cell.new(0, 0, false), Cell.new(0, 1, false),
                                             Cell.new(1, 0, false), Cell.new(1, 1, false)])
      end

      it 'with 1 live cell and 1 live neighbour kills the cell' do
        generation = Generation.new([Cell.new(0, 0, true), Cell.new(0, 1, true), Cell.new(0, 2, false),
                                     Cell.new(1, 0, false), Cell.new(1, 1, false), Cell.new(1, 2, false),
                                     Cell.new(2, 0, false), Cell.new(2, 1, false), Cell.new(2, 2, false)])
        next_generation = generation.evolve
        expect(next_generation.cells).to eq([Cell.new(0, 0, false), Cell.new(0, 1, false), Cell.new(0, 2, false),
                                             Cell.new(1, 0, false), Cell.new(1, 1, false), Cell.new(1, 2, false),
                                             Cell.new(2, 0, false), Cell.new(2, 1, false), Cell.new(2, 2, false)])
      end

      it 'having a dead cell with exactly three live neighbours revives the cell' do
        generation = Generation.new([Cell.new(0, 0, true), Cell.new(0, 1, true), Cell.new(0, 2, false),
                                     Cell.new(1, 0, true), Cell.new(1, 1, false), Cell.new(1, 2, false),
                                     Cell.new(2, 0, false), Cell.new(2, 1, false), Cell.new(2, 2, false)])
        next_generation = generation.evolve
        expect(next_generation.cells).to eq([Cell.new(0, 0, true), Cell.new(0, 1, true), Cell.new(0, 2, false),
                                             Cell.new(1, 0, true), Cell.new(1, 1, true), Cell.new(1, 2, false),
                                             Cell.new(2, 0, false), Cell.new(2, 1, false), Cell.new(2, 2, false)])
      end

      it 'with a live cell and two horizontally aligned live neighbours keeps the cell alive' do
        generation = Generation.new([Cell.new(0, 0, false), Cell.new(0, 1, true), Cell.new(0, 2, false),
                                     Cell.new(1, 0, false), Cell.new(1, 1, true), Cell.new(1, 2, false),
                                     Cell.new(2, 0, false), Cell.new(2, 1, true), Cell.new(2, 2, false)])
        next_generation = generation.evolve
        expect(next_generation.cells).to eq([Cell.new(0, 0, false), Cell.new(0, 1, false), Cell.new(0, 2, false),
                                             Cell.new(1, 0, true), Cell.new(1, 1, true), Cell.new(1, 2, true),
                                             Cell.new(2, 0, false), Cell.new(2, 1, false), Cell.new(2, 2, false)])
      end

      it 'with a live cell and four live neighbours kills the cell' do
        generation = Generation.new([Cell.new(0, 0, false), Cell.new(0, 1, true), Cell.new(0, 2, false),
                                     Cell.new(1, 0, true), Cell.new(1, 1, true), Cell.new(1, 2, true),
                                     Cell.new(2, 0, false), Cell.new(2, 1, true), Cell.new(2, 2, false)])
        next_generation = generation.evolve
        expect(next_generation.cells).to eq([Cell.new(0, 0, true), Cell.new(0, 1, true), Cell.new(0, 2, true),
                                             Cell.new(1, 0, true), Cell.new(1, 1, false), Cell.new(1, 2, true),
                                             Cell.new(2, 0, true), Cell.new(2, 1, true), Cell.new(2, 2, true)])
      end
    end

  end
end