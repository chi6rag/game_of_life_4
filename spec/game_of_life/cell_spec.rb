module GameOfLife
  RSpec.describe Cell do

    describe 'killing a cell' do
      it 'keeps the cell dead if the cell is dead already' do
        cell = Cell.new(1, 1, false)
        cell.die!
        expect(cell.is_alive).to eq(false)
      end

      it 'makes the cell dead if it is alive' do
        cell = Cell.new(1, 1, true)
        cell.die!
        expect(cell.is_alive).to eq(false)
      end
    end

    context 'when compared for equality with' do
      let(:cell){Cell.new(1, 1, false)}

      it 'nil is not equal' do
        expect(cell == nil).to eq(false)
      end

      it 'itself is equal' do
        expect(cell == cell).to eq(true)
      end

      it 'another object is not equal' do
        expect(cell == 1).to eq(false)
      end

      it 'another Cell of different states is not equal' do
        other_cell = Cell.new(2, 2, true)
        expect(cell == other_cell).to eq(false)
      end

      it 'another Cell of same states is equal' do
        other_cell = Cell.new(1, 1, false)
        expect(cell == other_cell).to eq(true)
      end

      it 'another Cell for symmetry is equal' do
        other_cell = Cell.new(1, 1, false)
        expect(other_cell == cell).to eq(true)
      end
    end

    it 'has the same hash as another Cell of same states' do
      cell = Cell.new(1, 1, false)
      other_cell = Cell.new(1, 1, false)
      expect(cell.hash == other_cell.hash).to eq(true)
    end

    context 'finds live neighbours' do
      it 'vertically above itself' do
        cell = Cell.new(1, 1, true)
        cells = [Cell.new(0, 0, false), Cell.new(0, 1, true), Cell.new(0, 2, false),
                 Cell.new(1, 0, false), Cell.new(1, 1, true), Cell.new(1, 2, false),
                 Cell.new(2, 0, false), Cell.new(2, 1, false), Cell.new(2, 2, false)]
        expect(cell.find_live_neighbours_among(cells)).to eq([Cell.new(0, 1, true)])
      end

      it 'vertically below itself' do
        cell = Cell.new(1, 1, true)
        cells = [Cell.new(0, 0, false), Cell.new(0, 1, false), Cell.new(0, 2, false),
                 Cell.new(1, 0, false), Cell.new(1, 1, true), Cell.new(1, 2, false),
                 Cell.new(2, 0, false), Cell.new(2, 1, true), Cell.new(2, 2, false)]
        expect(cell.find_live_neighbours_among(cells)).to eq([Cell.new(2, 1, true)])
      end

      it 'horizonatlly to right of itself' do
        cell = Cell.new(1, 1, true)
        cells = [Cell.new(0, 0, false), Cell.new(0, 1, false), Cell.new(0, 2, false),
                 Cell.new(1, 0, false), Cell.new(1, 1, true), Cell.new(1, 2, true),
                 Cell.new(2, 0, false), Cell.new(2, 1, false), Cell.new(2, 2, false)]
        expect(cell.find_live_neighbours_among(cells)).to eq([Cell.new(1, 2, true)])
      end

      it 'horizonatlly to left of itself' do
        cell = Cell.new(1, 1, true)
        cells = [Cell.new(0, 0, false), Cell.new(0, 1, false), Cell.new(0, 2, false),
                 Cell.new(1, 0, true), Cell.new(1, 1, true), Cell.new(1, 2, false),
                 Cell.new(2, 0, false), Cell.new(2, 1, false), Cell.new(2, 2, false)]
        expect(cell.find_live_neighbours_among(cells)).to eq([Cell.new(1, 0, true)])
      end

      it 'diagonally to itself' do
        cell = Cell.new(1, 1, true)
        cells = [Cell.new(0, 0, true), Cell.new(0, 1, false), Cell.new(0, 2, true),
                 Cell.new(1, 0, false), Cell.new(1, 1, true), Cell.new(1, 2, false),
                 Cell.new(2, 0, true), Cell.new(2, 1, false), Cell.new(2, 2, true)]
        expect(cell.find_live_neighbours_among(cells)).to eq([Cell.new(0, 0, true), Cell.new(0, 2, true),
                                                              Cell.new(2, 0, true), Cell.new(2, 2, true)])
      end
    end

    it 'finds no live neighbours if all other cells are dead' do
      cell = Cell.new(0, 0, true)
      cells = [Cell.new(0, 0, true), Cell.new(0, 1, false),
               Cell.new(1, 0, false), Cell.new(1, 1, false)]
      expect(cell.find_live_neighbours_among(cells)).to eq([])
    end

    describe 'reviving a cell' do
      it 'which is already alive keeps it alive' do
        cell = Cell.new(1, 1, true)
        cell.revive!
        expect(cell.is_alive).to eq(true)
      end

      it 'which is dead makes it alive' do
        cell = Cell.new(1, 1, false)
        cell.revive!
        expect(cell.is_alive).to eq(true)
      end
    end

  end
end