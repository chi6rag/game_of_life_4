module GameOfLife

  # Generation represents the collection of cells at a particular tick in the universe

  class Generation
    attr_reader :cells

    def initialize(cells)
      @cells = cells
    end

    def evolve
      next_generation_cells = copy_current_generation_cells
      next_generation_cells.each{|cell| apply_rules(cell)}
      Generation.new(next_generation_cells)
    end

    private
    def copy_current_generation_cells
      @cells.map { |cell| cell.clone }
    end

    def apply_rules(cell)
      live_neighbours = cell.find_live_neighbours_among(@cells)
      case (live_neighbours.count)
        when (0...2)
          cell.die!
        when (3)
          cell.revive! unless cell.is_alive
        when (4..8)
          cell.die!
      end
    end

  end
end