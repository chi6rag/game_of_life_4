module GameOfLife

  # Cell is an entity which represents life

  class Cell
    attr_reader :x, :y, :is_alive

    def initialize(x, y, is_alive)
      @x = x
      @y = y
      @is_alive = is_alive
    end

    def die!
      @is_alive = false
    end

    def revive!
      @is_alive = true
    end

    def ==(other)
      return false unless other.is_a?(Cell)
      return true if(has_same_x_coordinates_as?(other) &&
                     has_same_y_coordinates_as?(other) &&
                     has_same_life_state_as?(other))
      false
    end

    def eql?(other)
      self == other
    end

    def hash
      [@x, @y, @is_alive].hash
    end

    def find_live_neighbours_among(cells)
      res = cells.select{|cell| (self.x - cell.x).between?(-1, 1) &&
                                (self.y - cell.y).between?(-1, 1) &&
                                (cell.is_alive) }
      res.delete(self)
      res
    end

    private
    def has_same_x_coordinates_as?(other)
      @x == other.x
    end

    def has_same_y_coordinates_as?(other)
      @y == other.y
    end

    def has_same_life_state_as?(other)
      @is_alive == other.is_alive
    end

  end
end